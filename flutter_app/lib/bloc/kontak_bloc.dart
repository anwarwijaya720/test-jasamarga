import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gic/model/kontak_model.dart';
import 'package:flutter_gic/service/kontak_service.dart';
import 'package:flutter_gic/widget/alertdialog.dart';
import 'package:flutter_gic/widget/global_response.dart';
import 'package:flutter_gic/widget/loading_screen.dart';
import 'package:flutter_gic/widget/service_response.dart';
import 'package:rxdart/rxdart.dart';
import 'package:uuid/uuid.dart';

class KontakBloc{
    
  ProgressBar sendingMsgProgressBar = ProgressBar();
  int page = 1;
  var alert = CustomDialog();
  bool isLoading = false;
  var service = ServiceKontak();
  var uuid = Uuid();
  var txtTitleAlrt;
  var actionType;
  var fliter;  
  var transactionId;
  var idUpdateKontak;
  var searchController = TextEditingController();
  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var noHphController = TextEditingController();

  final _fetcherGetKontak = BehaviorSubject<List<KontakModel>>();
  Stream<List<KontakModel>> get getKontakList => _fetcherGetKontak.stream;

  getKontak(BuildContext context) async {
    print("service get");
    // sendingMsgProgressBar.show(context);
    List<KontakModel> result = await service.getServiceKontak(fliter, page);
    if(page == 1){
    _fetcherGetKontak.sink.add(result); 
    }else{
      if(result.length > 0){
        print("ada");
        _fetcherGetKontak.sink.add(_fetcherGetKontak.value + result);
      }else{        
        print("kosong");
        showSnackBar(context, 'data telah mencapai batas akhir');
      }
    }   
    // sendingMsgProgressBar.hide();
    isLoading = true;
  }

  actionTypeKontak(BuildContext context) async{
    if(actionType == "save"){
      transactionId = uuid.v4();
    }else{
      transactionId = idUpdateKontak;
    }

    var model = KontakModel(
        kontak_id: transactionId,
        nama: nameController.text.toString(),
        email: emailController.text.toString(),
        noHp: noHphController.text.toString());

    print(model.nama.toString());
    print(model.email.toString());
    print(model.kontak_id.toString());

    GlobalResponse response = await service.actionTypeService(model, actionType);     
    getKontak(context);

    print("response : "+ response.status.toString());     
    
    if (response.status == 200) {
      print('process successfully');      
      showSnackBar(context, '${actionType.toString()} data successfuly');
    } else {
      alert.showAlertDialog(context, "Alert", "process failed");
      print('process failed');
    }
  }

  deleteKontak(context, id) async{
    sendingMsgProgressBar.show(context);
    GlobalResponse response = await service.deleteService(id);     
    getKontak(context);       
    sendingMsgProgressBar.hide();
    if (response.status == 200) {
      print('process successfully');      
      showSnackBar(context, 'delete data successfuly');
    } else {
      alert.showAlertDialog(context, "Alert", "process failed");
      print('process failed');
    }
  }

  void showSnackBar(BuildContext context, String text) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text(text)));
  }

  void dispose() {
    _fetcherGetKontak.close();
  }

}