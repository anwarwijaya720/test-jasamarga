import 'package:flutter/material.dart';
import 'package:flutter_gic/model/user_jasamarga_model.dart';
import 'package:flutter_gic/service/user_jasamarga_service.dart';
import 'package:rxdart/rxdart.dart';

class UserJasamargaBloc{

  var service = UserJasamargaService();
  bool isLoading = false;

  final _fetcherUserJasamargaFollowers = BehaviorSubject<List<UserJasamargaModel>>();
  Stream<List<UserJasamargaModel>> get getUserJasamargaFollowers => _fetcherUserJasamargaFollowers.stream;

  final _fetcherUserJasamargaFollowing = BehaviorSubject<List<UserJasamargaModel>>();
  Stream<List<UserJasamargaModel>> get getUserJasamargaFollowing => _fetcherUserJasamargaFollowing.stream;
  
  void dispose() {
    _fetcherUserJasamargaFollowers.close();
    _fetcherUserJasamargaFollowing.close();
  }

  getFollowers(BuildContext context) async {    
    List<UserJasamargaModel> result = await service.getUserFollowersJasamargaService();
    print("response");    
    print(result);        
    _fetcherUserJasamargaFollowers.sink.add(result);
    isLoading = true;
  }

  getFollowing(BuildContext context) async {  
    List<UserJasamargaModel> result = await service.getUserFollowingJasamargaService();
    print("response");    
    print(result);        
    _fetcherUserJasamargaFollowing.sink.add(result);     
    isLoading = true;
  }  

}