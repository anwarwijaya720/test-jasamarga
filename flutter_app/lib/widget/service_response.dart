import 'package:json_annotation/json_annotation.dart';

class ServiceResponseKey{
  static String serviceResponseKey = 'serviceResponse';

  static String status = 'status';
  static String message = 'message';
}

@JsonSerializable()
class ServiceResponse {
  final int status;
  final String message;

  ServiceResponse(
    {
      this.status,
      this.message
    }
  );

  factory ServiceResponse.fromJson(Map<String, dynamic> json) {
    // print('response service : $json');
    return ServiceResponse(
      status: json[ServiceResponseKey.status] as int ?? 400,
      message: json[ServiceResponseKey.message] as String,
    );
  }

}
