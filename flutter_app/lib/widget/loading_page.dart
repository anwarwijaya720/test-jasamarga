import 'package:flutter/material.dart';

class LoadingPages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 80,
        height: 80,
        color: Colors.black,
        margin: EdgeInsets.only(top: 180.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(color: Colors.white),
            Padding(
                padding: EdgeInsets.only(top: 10),
                child: Text(
                  "Loading...",
                  style: TextStyle(color: Colors.white),
                ))
          ],
        ));
  }
}

class LoadingProcess extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    Container(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          child: Column(
            children: [
              CircularProgressIndicator(color: Colors.black),
              Padding(
              padding: EdgeInsets.only(top: 10),
              child: Text(
                "Loading...",
                style: TextStyle(color: Colors.black),
              )),
            ],
          ),
        )              
      ]),
    );
  }
}