
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class GlobalResponse{
  final int status;
  final String message;

  GlobalResponse({
    this.status, 
    this.message
  });

  factory GlobalResponse.fromJson(Map<String, dynamic> json) {
    return GlobalResponse(
      status: json['status'] as int,
      message: json['message'] as String,
    );
  }

}