import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomDialog {  

  showAlertDialog(BuildContext context, txtTitle, txtcontent) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {         
        // Navigator.of(context).pop();
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(txtTitle ?? ''),
      content: Text(txtcontent ?? ''),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

}
