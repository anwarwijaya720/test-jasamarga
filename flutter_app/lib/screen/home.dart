import 'package:flutter/material.dart';
import 'package:flutter_gic/bloc/kontak_bloc.dart';
import 'package:flutter_gic/model/kontak_model.dart';
import 'package:flutter_gic/widget/loading_page.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class KontakScreen extends StatefulWidget {
  @override
  _KontakScreenState createState() => _KontakScreenState();
}

class _KontakScreenState extends State<KontakScreen> {
  ScrollController scrollController = ScrollController(
    initialScrollOffset: 0.0,
    keepScrollOffset: true,
  );
  var bloc = KontakBloc();

  @override
  void initState() {
    super.initState();
    main();
    scrollController.addListener(() {
      if(scrollController.position.pixels == scrollController.position.maxScrollExtent){
        bloc.page ++;
        print("total page : "+bloc.page.toString());
        main();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  void main() {
    setState(() {
      bloc.getKontak(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Contact List",
          style: TextStyle(fontSize: 16),
        ),
      ),
      body: 
      // streamKontak(),
      SingleChildScrollView(        
        controller: scrollController,
        child: Container(
          margin: EdgeInsets.all(10),
          child: Column(
            children: [
              searchBox(),
              streamKontak()               
            ],
          ),
        ),
      ),
      floatingActionButton: Container(
        child: FloatingActionButton(
          backgroundColor: Colors.red,
          onPressed: () {
            bloc.actionType = "save";            
            bloc.txtTitleAlrt = "Add User";
            bloc.nameController.clear();
            bloc.emailController.clear();
            bloc.noHphController.clear();
            showAlertDialog(context);
          },
          tooltip: 'Add',
          child: Icon(Icons.add),
        ),
      ),
    );
  }

  Widget searchBox() {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextFormField(
        controller: bloc.searchController,
        decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.blue, width: 2.0),
              borderRadius: BorderRadius.circular(20.0),
            ),
            hintText: 'Search...',
            prefixIcon: Icon(Icons.search)),
        onChanged: (val) {
          print(val.toString());
          if (val.length >= 2) {
            bloc.fliter = val;
            bloc.page = 1;
            bloc.getKontak(context);
          } else {
            bloc.fliter = null;                       
            main();            
          }
        },
      ),
    );
  }

  Widget streamKontak() {
    return StreamBuilder(
      stream: bloc.getKontakList,
      builder: (context, AsyncSnapshot<List<KontakModel>> snapshot) {
        print(snapshot.data);
        if (bloc.isLoading == false) {
          return LoadingPages();
        } else {
          if (snapshot.hasData) {
            if (snapshot.data != null) {
              print(snapshot.data);
              return
              listBox(snapshot.data);
            } else {
              return Container(
                margin: EdgeInsets.only(top: 180),
                child: Center(
                  child: Column(
                    children: [
                      Icon(Icons.warning, size: 70,),
                      Text("no data contact")
                    ],
                  )
                ),
              );
            }
          } else {
            return Container(
            margin: EdgeInsets.only(top: 180),
            child: Center(child: Column(
              children: [
                Icon(Icons.warning, size: 70,),
                Text("no data fount")
              ],
            )));
          }
        }
      });
  }

  Widget listBox(List<KontakModel> model) {
    return ListView.builder(
        shrinkWrap: true,
        // controller: scrollController,
        itemCount: model.length,
        physics: ClampingScrollPhysics(),
        itemBuilder: (context, index) {
          return kontakList(model[index]);
        });
    // return Column(
    //     children: <Widget>[
    //       Expanded(
    //         child: NotificationListener<ScrollNotification>(
    //           onNotification: (ScrollNotification scrollInfo) {
    //             if (!bloc.isLoading && scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {                  
    //               // start loading data
    //               setState(() {
    //                 bloc.isLoading = true;
    //               });
    //             }
    //           },
    //           child:ListView.builder(
    //             shrinkWrap: true,
    //             itemCount: model.data.length,
    //             itemBuilder: (context, index) {
    //               bloc.isLoading = false;
    //               return kontakList(model.data[index]);
    //             },
    //           ),
    //         ),
    //       ),
    //       Container(
    //         height: bloc.isLoading ? 50.0 : 0,
    //         color: Colors.transparent,
    //         child: Center(
    //           child: new CircularProgressIndicator(),
    //         ),
    //       ),
    //     ],
    //   );
  }

  Widget loading(){
    return Container(
      height: 50.0,
      color: Colors.transparent,
      child: Center(
        child: new CircularProgressIndicator(),
      ),
    );
  }

  Widget kontakList(KontakModel model) {
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.25,
      child: Card(
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: [
              Text(model.nama),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text(model.email), Text(model.noHp)],
              ),
            ],
          ),
        ),
      ),
      secondaryActions: <Widget>[
        IconSlideAction(
          caption: 'Edit',
          color: Colors.grey.shade200,
          icon: Icons.edit,
          onTap: () {
            bloc.actionType = "update";                  
            bloc.txtTitleAlrt = "Update Contact";
            bloc.idUpdateKontak = model.kontak_id;
            bloc.nameController.text = model.nama;
            bloc.emailController.text = model.email;
            bloc.noHphController.text = model.noHp;
            showAlertDialog(context);
          },
          closeOnTap: false,
        ),
        IconSlideAction(
          caption: 'Delete',
          color: Colors.red,
          icon: Icons.delete,
          onTap: () {
            bloc.deleteKontak(context, model.kontak_id);
          },
        ),
      ],
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget saveButton = Container(
      child: FlatButton(
        child: Text("Save"),
        onPressed: () {
          bloc.actionTypeKontak(context);          
          Navigator.of(context).pop();
        },
      ),
    );

    // set up the button
    Widget closeButton = Container(
      child: FlatButton(
        child: Text("Close"),
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(bloc.txtTitleAlrt),
      content: SingleChildScrollView(
        child: Container(
        height: MediaQuery.of(context).size.height * 0.40,
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(10.0),
              child: new TextFormField(
                controller: bloc.nameController,
                style: TextStyle(color: Colors.black),
                decoration: new InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Colors.black, width: 2.0),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  hintText: 'Name',
                  hintStyle: TextStyle(color: Colors.black),
                  prefixIcon: const Icon(
                    Icons.person,
                    color: Colors.black,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    borderSide: BorderSide(
                      color: Colors.black,
                      width: 2.0,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10.0),
              child: new TextFormField(
                controller: bloc.emailController,
                keyboardType: TextInputType.emailAddress,
                style: TextStyle(color: Colors.black),
                decoration: new InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Colors.black, width: 2.0),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  hintText: 'Email',
                  hintStyle: TextStyle(color: Colors.black),
                  prefixIcon: const Icon(
                    Icons.email,
                    color: Colors.black,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    borderSide: BorderSide(
                      color: Colors.black,
                      width: 2.0,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10.0),
              child: new TextFormField(
                controller: bloc.noHphController,
                style: TextStyle(color: Colors.black),
                keyboardType: TextInputType.number,
                decoration: new InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Colors.black, width: 2.0),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  hintText: 'No Henphone',
                  hintStyle: TextStyle(color: Colors.black),
                  prefixIcon: const Icon(
                    Icons.phone,
                    color: Colors.black,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    borderSide: BorderSide(
                      color: Colors.black,
                      width: 2.0,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      ),
      actions: [
        Container(
          margin: EdgeInsets.only(left: 20, right: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              saveButton,
              closeButton,
            ],
          ),
        )
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

}
