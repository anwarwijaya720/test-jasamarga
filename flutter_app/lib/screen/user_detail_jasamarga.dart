import 'package:flutter/material.dart';
import 'package:flutter_gic/model/user_jasamarga_model.dart';

class UserDetail extends StatefulWidget {
  final UserJasamargaModel modelUsers;

  const UserDetail({Key key, this.modelUsers}) : super(key: key);

  @override
  _UserDetailState createState() => _UserDetailState();
}

class _UserDetailState extends State<UserDetail> {

  UserJasamargaModel model;

  @override
  void initState() {
    super.initState();
    model = widget.modelUsers;
  }

  @override
  void dispose() {
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( 
        title: Text("Detail Users", style: TextStyle(fontSize: 18.0),),
        backgroundColor: Colors.blueGrey,      
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.only(top: 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(50.0),
              child: Image.network(
                model.avatarUrl,
                width: 200,
                height: 200,
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 10.0)),
            Text(model.login.toString(), style: TextStyle(fontSize: 18.0),),
            Padding(padding: EdgeInsets.only(top: 10.0)),
            Text(model.id.toString(), style: TextStyle(fontSize: 16.0),),
            Padding(padding: EdgeInsets.only(top: 10.0)),
            Text(model.type.toString(), style: TextStyle(fontSize: 16.0),)
          ],
        ),
      ),      
    );
  }
}