import 'package:flutter/material.dart';
import 'package:flutter_gic/bloc/user_jasamarga_bloc.dart';
import 'package:flutter_gic/model/user_jasamarga_model.dart';
import 'package:flutter_gic/screen/user_detail_jasamarga.dart';
import 'package:flutter_gic/widget/loading_page.dart';

class TestJasamarga extends StatefulWidget {
  @override
  _TestJasamargaState createState() => _TestJasamargaState();
}

class _TestJasamargaState extends State<TestJasamarga> {

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKeyFollowers = new GlobalKey<RefreshIndicatorState>(); 
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKeyFollowing = new GlobalKey<RefreshIndicatorState>();  
  var bloc = UserJasamargaBloc();

  @override
  void initState() {
    super.initState();
    main();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  void main(){
    setState(() {
      bloc.getFollowers(context);
      bloc.getFollowing(context);
    });
  }

  Future<Null> _refreshFollowers() async {
    setState(() {
      bloc.getFollowers(context);
    });
  }

  Future<Null> _refreshFollowing() async {
    setState(() {
      bloc.getFollowing(context);
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return
    DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Github User's", style: TextStyle(fontSize: 18),),
          backgroundColor: Colors.blueGrey,
          elevation: 0,
          bottom: TabBar(
            unselectedLabelColor: Colors.black,
            indicatorSize: TabBarIndicatorSize.tab,
            indicator: BoxDecoration(
            gradient: LinearGradient(
            colors: [Colors.redAccent, Colors.orangeAccent]),
            borderRadius: BorderRadius.circular(50),
            color: Colors.redAccent),
            tabs: [
              Tab(
                child: Align(
                  alignment: Alignment.center,
                  child: Text("Followers"),
                ),
              ),
              Tab(
                child: Align(
                  alignment: Alignment.center,
                  child: Text("Following"),
                ),
              ),
            ]),
        ),
        body: TabBarView(
        children: [
          RefreshIndicator(
            key: _refreshIndicatorKeyFollowers,
            onRefresh: _refreshFollowers,
            child: listStreamFLollowers(),
          ),
          RefreshIndicator(
            key: _refreshIndicatorKeyFollowing,
            onRefresh: _refreshFollowing,
            child: listStreamFollowing(), 
          ),
        ]),
      )
    );
  }

  Widget listStreamFLollowers(){
    return StreamBuilder(
      stream: bloc.getUserJasamargaFollowers,
      builder: (context, AsyncSnapshot<List<UserJasamargaModel>> snapshot) {
        print(snapshot.data);
        if (bloc.isLoading == false) {
          return Center(child: LoadingProcess());
        } else {
          if (snapshot.hasData) {
            if (snapshot.data != null) {
              print(snapshot.data);
              return
              listFLollowers(snapshot.data);
            } else {
              return Container(
                margin: EdgeInsets.only(top: 180),
                child: Center(
                  child: Column(
                    children: [
                      Icon(Icons.warning, size: 70,),
                      Text("no data user")
                    ],
                  )
                ),
              );
            }
          } else {
            return Container(
            margin: EdgeInsets.only(top: 180),
            child: Center(child: Column(
              children: [
                Icon(Icons.warning, size: 70,),
                Text("no data fount")
              ],
            )));
          }
        }
      }
    );
  }

  Widget listFLollowers(List<UserJasamargaModel> model){
    return ListView.separated(
      itemCount: model.length,
      itemBuilder: (context, index){
        return GestureDetector(
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => UserDetail(modelUsers: model[index])),
            );
          },
          child: Container(
            margin: EdgeInsets.all(10.0),
            width: MediaQuery.of(context).size.width,            
            child: Row(
              children: [
                Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(50)
                  ),              
                  child: 
                  ClipRRect(
                    borderRadius: BorderRadius.circular(50.0),
                    child: Image.network(
                      model[index].avatarUrl,
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.only(right: 10.0)),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(model[index].login.toString()),
                    Padding(padding: EdgeInsets.only(top: 5)),
                    Text(model[index].type.toString())
                  ],
                )
              ],
            ),
          ),
        );
      },      
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }

  Widget listStreamFollowing(){
    return StreamBuilder(
      stream: bloc.getUserJasamargaFollowing,
      builder: (context, AsyncSnapshot<List<UserJasamargaModel>> snapshot) {
        print(snapshot.data);
        if (bloc.isLoading == false) {
          return Center(child: LoadingProcess());
        } else {
          if (snapshot.hasData) {
            if (snapshot.data != null) {
              print(snapshot.data);
              return
              listFollowing(snapshot.data);
            } else {
              return Container(
                margin: EdgeInsets.only(top: 180),
                child: Center(
                  child: Column(
                    children: [
                      Icon(Icons.warning, size: 70,),
                      Text("no data user")
                    ],
                  )
                ),
              );
            }
          } else {
            return Container(
            margin: EdgeInsets.only(top: 180),
            child: Center(child: Column(
              children: [
                Icon(Icons.warning, size: 70,),
                Text("no data fount")
              ],
            )));
          }
        }
      }
    );
  }

  Widget listFollowing(List<UserJasamargaModel> model){
    return ListView.separated(
      itemCount: model.length,
      itemBuilder: (context, index){
        return 
        GestureDetector(
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => UserDetail(modelUsers: model[index])),
            );
          },
          child: Container(
            margin: EdgeInsets.all(10.0),
            width: MediaQuery.of(context).size.width,
            child: Row(
              children: [
                Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(50)
                  ),              
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50.0),
                    child: Image.network(
                      model[index].avatarUrl,
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.only(right: 10.0)),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(model[index].login.toString()),
                    Padding(padding: EdgeInsets.only(top: 5)),
                    Text(model[index].type.toString())
                  ],
                )
              ],
            ),
          ),
        );        
      },      
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }

}