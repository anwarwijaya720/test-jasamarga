import 'package:json_annotation/json_annotation.dart';

class UserJasamargaModel{
  final String login;
  final int id;
  final String nodeId;
  final String avatarUrl;
  final String type;

  UserJasamargaModel({
    this.login, this.id, this.nodeId, this.avatarUrl, this.type
  });

  factory UserJasamargaModel.fromJson(Map<String, dynamic> json) {
    return UserJasamargaModel(
      login: json['login'] as String,
      id: json['id'] as int,
      nodeId: json['node_id'] as String,
      avatarUrl: json['avatar_url'] as String,
      type: json['type'] as String,
    );
  }

}

// @JsonSerializable()
// class UserJasamrgaModelResponse {

//   final List<UserJasamargaModel> data;

//   UserJasamrgaModelResponse(
//     {
//       this.data,
//     }
//   );

//   factory UserJasamrgaModelResponse.fromJson(Map<String, dynamic> json) {
//     return UserJasamrgaModelResponse(
//       data: (json as List).map((i) => new UserJasamargaModel.fromJson(i)).toList(),
//     );
//   }
// }