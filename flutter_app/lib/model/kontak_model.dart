import 'package:flutter_gic/widget/service_response.dart';
import 'package:json_annotation/json_annotation.dart';

class KontakModel{

  final String kontak_id;
  final String nama;
  final String noHp;
  final String email;

  KontakModel({
    this.kontak_id, 
    this.nama, this.noHp, this.email
  });

  factory KontakModel.fromJson(Map<String, dynamic> json) {
    return KontakModel(
      kontak_id: json['kontak_id'] as String,
      nama: json['name'] as String,
      email: json['email'] as String,
      noHp: json['no_hp'] as String,
    );
  }
  
  toLowerCase() {}

}

@JsonSerializable()
class KontakModelResponse {
  // final ServiceResponse serviceResponse; 
  // final int status;
  // final String message; 
  final List<KontakModel> data;

  KontakModelResponse(
    {
      // this.serviceResponse, 
      // this.status,
      // this.message,
      this.data,
    }
  );

  factory KontakModelResponse.fromJson(Map<String, dynamic> json) {
    return KontakModelResponse(
      // serviceResponse: ServiceResponse.fromJson(json[ServiceResponseKey.serviceResponseKey]),             
      // status: json['status'] as int,
      // message: json['message'] as String,
      data: (json['data'] as List).map((i) => new KontakModel.fromJson(i)).toList(),
    );
  }
}