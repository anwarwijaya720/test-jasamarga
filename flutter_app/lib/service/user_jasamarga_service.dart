import 'package:flutter_gic/model/user_jasamarga_model.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class UserJasamargaService{

  var token = "token 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";

  Future<List<UserJasamargaModel>> getUserFollowersJasamargaService() async {    
    var url = "https://api.github.com/users/mohamaddenisjs/followers";    
    Map<String, String> headers = {
      "content-type": "application/json",
      "Authorization" : token,
    };    
    print("service get followers");
    print("url get :"+url);
    final response = await http.get(Uri.parse(url), headers: headers);
    print(response.body);
    if (response.statusCode == 200) {
      print("oke");
      var body = jsonDecode(response.body);
      // var kontak = UserJasamargaModel.fromJson(body);
      // return kontak.data;
      return (body as List).map((p) => UserJasamargaModel.fromJson(p)).toList();
    } else {
      print("gagal");
      throw Exception('Failed to load booking');
    }
  }

  Future<List<UserJasamargaModel>> getUserFollowingJasamargaService() async {    
    var url = "https://api.github.com/users/mohamaddenisjs/following";    
    Map<String, String> headers = {
      "content-type": "application/json",
      "Authorization" : token,
    };
    print("service get following");
    print("url get :"+url);
    final response = await http.get(Uri.parse(url), headers: headers);
    print(response.body);
    if (response.statusCode == 200) {
      var body = jsonDecode(response.body);
      // var kontak = UserJasamrgaModelResponse.fromJson(body);
      return (body as List).map((p) => UserJasamargaModel.fromJson(p)).toList();;
    } else {
      throw Exception('Failed to load booking');
    }
  }

}