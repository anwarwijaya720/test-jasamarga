import 'package:flutter_gic/model/kontak_model.dart';
import 'package:flutter_gic/widget/global_response.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class ServiceKontak {

  var url = 'http://192.168.43.245/api-php-gic/api-besic/';

  Future<List<KontakModel>> getServiceKontak(data, page) async {    
    var urlGet;
    if(data != null){
      urlGet = url+"get_limit.php?where="+data+"&limit="+page.toString();
    }else{
      urlGet = url+"get_limit.php?where=&limit="+page.toString();
    }
    print("service get");
    print("url get :"+urlGet);
    Map<String, String> headers = {
      "content-type": "application/json",
    };
    final response = await http.get(Uri.parse(urlGet), headers: headers);
    print(response.body);
    if (response.statusCode == 200) {
      var body = jsonDecode(response.body);
      var kontak = KontakModelResponse.fromJson(body);
      return kontak.data;
      // return (body as List).map((p) => KontakModel.fromJson(p)).toList();
    } else {
      throw Exception('Failed to load booking');
    }
  }

  Future<GlobalResponse> actionTypeService(KontakModel model, actionType) async {
    print("service add");
    String type;
    if(actionType == "save"){
      type = "add.php";
    }else{
      type = "update.php";
    }
    Map<String, String> headers = {
      "content-type": "application/json",
    };
    var body = new Map<String, dynamic>();
    body = {
      "kontak_id": model.kontak_id,
      "name": model.nama,
      "no_hp": int.parse(model.noHp),
      "email": model.email
    };
    final response = await http.post(
        Uri.parse(url+type),
        headers: headers, 
        body: jsonEncode(body)
      );
    
    print(response.body);
    if (response.statusCode == 200) {          
      var body = jsonDecode(response.body);      
      var res = GlobalResponse.fromJson(body);      
      return res;
    } else {
      print("gagal");
      throw Exception('Failed to load booking');
    }
  }

  deleteService(transactionId) async{
    print("service delete");    
    Map<String, String> headers = {
      "content-type": "application/json",
    };
    final response = await http.post(
        Uri.parse(url+"delete.php?kontak_id="+transactionId),
        headers: headers,
      );
    
    print(response.body);
    if (response.statusCode == 200) {          
      var body = jsonDecode(response.body);      
      var res = GlobalResponse.fromJson(body);      
      return res;
    } else {
      print("gagal");
      throw Exception('Failed to load booking');
    }

  }

}
