import 'package:flutter/material.dart';
import 'package:flutter_gic/screen/home.dart';
import 'package:flutter_gic/screen/user_jasamarga.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      debugShowCheckedModeBanner: false,
      // home: KontakScreen(),
      home: TestJasamarga(),
    );
  }
}
